<link  rel="stylesheet" href="style.css" />

# MI4 Architecture Dev Ops

Cette page et les slides sont sous licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/fr/), le code source est sous licence [GPL V3 ou supérieur](https://www.gnu.org/licenses/gpl-3.0.html)

Cette page présente la partie conteneurisation et déploiement descriptif de l'UE Architecture Dev OPS

Pour toute question envoyer un mail à [David Beniamine](mailto:david.beniamine@univ-grenoble-alpes.fr)

## Organisation des cours / TP

+ Présentation puis TP
+ Questions dans les slides
+ Compte rendus :
    + Par bi/trinôme
    + À déposer sur [Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php)
    + **évalué, à rendre avant minuit le soir de la séance**

### Attention

+ Ne téléchargez pas les slides trop en avance ils peuvent changer jusqu'au dernier moment
+ Les énoncés donnent souvent des indications sur les questions précédentes
+ Prenez le temps de tester avant d'avancer

## Programme et slides

+  05/12/22
    + 3h
    + [Introduction aux conteneurs](intro.pdf)
+ 07/12/22
    + 3h
    + [Processus de compilation et de publication de conteneur avec docker](build.pdf)
+ 04/01/23
    + 6h
    + [Orchestration de conteneur avec docker-compose](orchestration.pdf)
    + [Ouverture](ouverture.pdf) (retour sur le projet, mes usages, outils pour la prod, réponses à vos questions)
+ 30/01/23
    + 6h
    + [mini projet](projet.pdf)
    + Évaluation par les pairs

## Évaluation

+ 3 intéros flash début de cours (5min **au début du cours**)
    + 07/12/22 (sur le cours introduction)
    + 04/01/23 (Sur le cours compilation)
    + 30/01/23 (Sur le cours orchestration)
+ 3 CR de TP
    + Introduction aux conteneurs
    + Processus de compilation
    + Orchestration
+ 1 mini projet
    + Évalué sur la facilité de déploiement

## Ressources

Pour utiliser / comprendre Docker : [la documentation docker](https://docs.docker.com/).

C'est en lisant du code qu'on apprend, donc voici des exemple concret de mon utilisation professionnelle de Docker:

+ Ce dépôt (slides et page web) :
    + [Source et surtout gitlab-ci.yml](https://gricad-gitlab.univ-grenoble-alpes.fr/beniamid/mi4_architecture_dev_ops)
    + [Image utilisée](https://gitlab.tetras-libre.fr/dbeniamine/gitlab-ci-pandoc)
+ Application web:
    + [LabNbook](https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/)
        + pile `LAMP`
        + Ajout de chromium + configuration pour tests automatiques codeception
        + Voir `docker-compose.yml` et dossier `docker`
    + [unl-demo](https://gitlab.tetras-libre.fr/unl/web-service-demo)
        + Mini site de demo en flask (python) qui exécute du java
        + Voir le `Dockerfile`
    + [Tétras Lab](https://gitlab.tetras-libre.fr/tetras-libre/jupyter/tetras-lab)
        + Pile complexe avec
            + Frontent
            + Plusieurs applicatifs
            + Plusieurs bases de données
            + Extensibilité via `COMPOSE_FILE` et via un mécanisme "custom" de `FLAVORS`
        + Voir le `docker-compose.yml`
+ [Image pour visualiser des docker-compose](https://hub.docker.com/repository/docker/dbeniamine/docker-compose-viz-mermaid) exemple `docker run --rm -it -v $PWD:/data dbeniamine/docker-compose-viz-mermaid -M -otest.png`
