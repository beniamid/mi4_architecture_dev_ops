---
title: Ouverture
institute: IU2 MI4 Architecture Dev ops
author:
    - David Beniamine
date: 04/01/23
toc: true
toc-title: Sommaire
Licence: CC-BY-SA
aspectratio: 169
theme: Boadilla
header-includes: |
    \usepackage{textpos}
    \usepackage{booktabs}
    \usepackage[labelformat=empty]{caption}
    \useinnertheme[shadow]{rounded}
    \useoutertheme[subsection=false]{smoothbars}
    \definecolor{iutred}{HTML}{F2590D}
    \definecolor{keyword}{HTML}{007021}
    \hypersetup{
        colorlinks=true, %colorise les liens
        breaklinks=true, %permet le retour à la ligne dans les liens trop longs
        urlcolor= blue, %couleur des hyperliens
        linkcolor= iutred, %couleur des liens internes
        citecolor=blue,	 %couleur des liens de citations
        bookmarksopen=true,
        pdftoolbar=false,
        pdfmenubar=false,
    }
    \graphicspath{{img/}}
    \titlegraphic{\includegraphics[height=2cm]{logo.jpg}}
    \addtobeamertemplate{frametitle}{}{%
    \begin{textblock*}{90mm}(.85\textwidth,-.8cm)
        \includegraphics[width=2cm]{logo.jpg}
    \end{textblock*}}
    \usecolortheme[named=iutred]{structure}
    \usecolortheme{beaver}
    \usepackage{tikz}
    \usetikzlibrary{arrows, positioning, calc}
    \tikzset{%
      % Specifications for style of nodes:
                base/.style = {rectangle, rounded corners, draw=black,
                               minimum height=1cm,
                               text centered, font=\sffamily},
        host/.style = {base, fill=blue!30, minimum width=4.5cm},
        guest/.style = {base, minimum width=1cm, fill=green!30},
        mount/.style = {base, fill=orange!30},
        file/.style = {host, fill=orange!30},
        remote/.style = {base, fill=brown},
        hash/.style = {orange!80},
    }
    \AtBeginSubsection{}
    \makeatletter
    \setbeamertemplate{footline}{\hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1em}}
    \makeatother
---

# Introduction

### Retour sur le projet

* Notes en lignes
* Remarques globales
* Questions particulières en fin de séance

### Rappels conteneurs

#### Principes

+ Minimaliste
+ Isolation
+ Environnement contrôlé
+ Configurable par environnement

. . .

#### Utilité en Dev Ops

+ Approche micro services
+ Descriptif
+ Orchestration

# Sécurité

### Mises à jour

\begin{exampleblock}{The good (avantages)}
    \begin{itemize}
        \item \texttt{docker-compose pull}
        \item \texttt{docker-compose down \&\& docker-compose up -d}
    \end{itemize}
\end{exampleblock}

. . .

\begin{block}{The bad (limites)}
    \begin{itemize}
        \item Parfois plus compliqué : changement de version BD etc.
        \item Besoin de faire confiance aux images upstream
    \end{itemize}
\end{block}

. . .

\begin{alertblock}{The ugly (presque impossible)}
    \begin{itemize}
        \item Lister les services sont impactés par une faille de sécurité
    \end{itemize}
\end{alertblock}



### Menaces propres à docker

* Images de base non mises à jour
* **Noyau partagé** avec l'hôte : attention à la sécurité de l'hôte
+ Un utilisateur membre du groupe `docker` peut devenir `root` sur **tous** les conteneurs
* `docker` ouvre des ports avec `iptables` ce qui peut contourner votre parfeu

### Recommandations

* Choix des images (voir cours sur `Dockerfile`)
* Anticiper les procédures de mises à jour
* Limiter les accès au serveur de base au strict minimum
* Ne **jamais** exposer un port interne
* Utiliser un point d'entrée unique (cf exemple avec Træfik dans les prochains slides)

# Outils pour la prod

## Bonnes pratiques


### Astuces / recommandations

1. Versionner
    * Mettre vos `docker-compose.yml` dans des dépôts git
    * 1 docker-compose = 1 git
    * Pas de configuration user dans le git
2. Rassembler les `docker-compose`
3. Mettre des scripts standardisés et les appeler dans vos procédures globales
    * `backup.sh` : dump les bases de données du `docker-compose`
    * `Upgrade.sh` si d'autres actions que pull et up / down sont nécessaire
4. Dans vos images prévoir une variable `ENV` qui sépare le mode `prod` du mode `dev`
    * Choix du serveur web (serveur interne du framework / vrai serveur de prod)
    * Configurations de debugs
    * etc.
5. Utiliser `COMPOSE_FILE` pour séparer les configurations de bases des options, en particulier pours les ports et les services "d'aide au développement"

## Reverse proxy

### Reverse proxy

:::{.columns}
:::{.column width=50%}


```{.mermaid width=500 caption="Proxy standard"}
graph LR

subgraph reseau local
Client
Proxy
end

Client --> Proxy --> internet(Internet) --> server[Server Web]
```

. . .

:::
:::{.column width=50%}


```{.mermaid width=500 caption="Reverse proxy"}
graph LR

subgraph reseau interne
Proxy
app1
app2
end

Client --> internet(Internet) --> Proxy --> app1[Server Web 1]
Proxy --> app2[Server Web 2]
```

:::
:::

. . .

#### Utilité

+ Plusieurs services derrières un même point d'entrée
* Sécurité : un seul point d'entrée
+ Équilibrage de charge / redondance
+ N'importe quel serveur web peut être un reverse proxy


### Træfik

+ Reverse proxy très léger
+ Les services web s'enregistrent auprès de Træfik **sans redémarrage du reverse proxy**
+ Génération automatiques de certificats ssl let's encrypt au premier accès
+ Conçu pour les micros services
+ Notion de middleware (ajout d'header, redirections etc.)
+ Unification des logs d'accès: facilite le fail2ban
+ Tableaux de bords

### Traefik dans docker

+   Ajouter des labels dans le service `docker-compose.yml` pour rendre un service accessible :
```yml
labels:
  - "traefik.enable=true"
  - "traefik.docker.network=traefik"
  - "traefik.http.routers.${NAME}.rule=Host(`${HOST}`)"
  - "traefik.http.routers.${NAME}.tls.certresolver=myresolver"
  - "traefik.http.routers.${NAME}.entrypoints=web,websecure"
  - "traefik.http.routers.${NAME}.middlewares=hardening@docker"
networks:
  - traefik
```
+ Pas besoin de binder de port sur l'hôte a part pour Træfik
+   [Ma config Traefik de base](https://gitlab.tetras-libre.fr/nocloud/docker/traefik)

### Routeurs Træfik

\begin{center}
\includegraphics[height=\textheight,keepaspectratio]{traefik_routeur.png}
\end{center}

### Détail d'un service

\begin{center}
\includegraphics[height=\textheight,keepaspectratio]{traefik_details.png}
\end{center}

# Mes utilisations de docker

### Tests de compatibilité

#### Objectif
+ Lancer très rapidement un environnement minimaliste pour reproduire un bug
+ Typiquement essayer un script avec une version php, python ou autre différente de celle(s) qu'on a en local

. . .

#### Exemple
```bash
docker run --rm -it php:<ver>-cli -v $PWD:target bash
```

. . .

#### Remarques
On peut utiliser cela en CI (voir prochain slide) pour lancer les tests sur plusieurs versions du language


### Gitlab-ci

::: {.columns}
::: {.column width="48%"}

#### `.gitlab-ci.yml`: page et slides
```yml
image: dbeniamine/gitlab-ci-pandoc
pages:
  stage: deploy
  script:
  - mkdir public
  - cd src
  - make
  - cp web/*.{css,html} ../public
  - cp slides/*.pdf ../public
  artifacts:
    paths:
      - public
```

:::
::: {.column width="48%"}

. . .

#### `.gitlab-ci.yml`: tests d'une app django
```yaml
image: python:3.7
services:
    - mariadb
variables:
    MYSQL_ROOT_PASSWORD: password
    MYSQL_PASSWORD: password
    MYSQL_USER: user
    MYSQL_DATABASE: app
test:
  script:
    - pip install -r requirements.txt
    - ./init_test_db.sh
    - python manage.py test
```

:::
:::


### Gitlab-ci multiples version de python


#### `.gitlab-ci.yml`
```yaml
before_script:
  - pip install -e .

test-37:
  image: python:3.7
  script:
   - python setup.py test

test-38:
  image: python:3.8
  script:
   - python setup.py test
```

### LabNbook

* Appli web classique
* `docker` uniquement utilisé pour le développement
* 3 services
    * `mariadb`
    * `phpmyadmin`
    * `front`
        * Apache configuré en mode debug
        * Chromium pour test selenium (clicks programmés)
        * Lancement migrations et dépendances au démarrage

### Tétras Lab (developpement)

```{.mermaid width=1200}
graph LR
  nginx -->|ws://.../voila/api/kernels?token| voila
  nginx -->|ws://.../api/kernels?token| jupyter
  nginx -->|http://../| django(django with auth)
  django -->|http://../dashboards/id| voila
  django -->|http://../admin/jupyter| jupyter(jupyter lab)
  django -->|http://../dashboards/id| nbviewer
  django --> worker(django worker)
  django --> mariadb[(mariadb for django)]
  voila --> localDB[(local DB for dashboards)]
  jupyter --> localDB
  worker --> localDB
  worker --> mariadb
```

### Tétras Lab (production)

::: {.columns}
::: {.column width="10%"}

:::
::: {.column width="90%"}

```{.mermaid width=1400}
graph LR

subgraph client0
nginx0
voila0
jupyter0
django0
nbviewer0
localDB0
mariadb0
worker0
end

nginx0[nginx] -->|ws://.../voila/api/kernels?token| voila0[voila]
nginx0 -->|ws://.../api/kernels?token| jupyter0
nginx0 -->|http://../| django0(django with auth)
django0 -->|http://../dashboards/id| voila0
django0 -->|http://../admin/jupyter| jupyter0(jupyter lab)
django0 -->|http://../dashboards/id| nbviewer0[nbviewer]
django0 --> worker0(django worker)
django0 --> mariadb0[(mariadb)]
voila0 --> localDB0[(local DB)]
jupyter0 --> localDB0
worker0 --> localDB0
worker0 --> mariadb0

subgraph client1
nginx1
voila1
jupyter1
django1
nbviewer1
localDB1
mariadb1
worker1
end

nginx1[nginx] -->|ws://.../voila/api/kernels?token| voila1[voila]
nginx1 -->|ws://.../api/kernels?token| jupyter1
nginx1 -->|http://../| django1(django with auth)
django1 -->|http://../dashboards/id| voila1
django1 -->|http://../admin/jupyter| jupyter1(jupyter lab)
django1 -->|http://../dashboards/id| nbviewer1[nbviewer]
django1 --> worker1(django worker)
django1 --> mariadb1[(mariadb)]
voila1 --> localDB1[(local DB)]
jupyter1 --> localDB1
worker1 --> localDB1
worker1 --> mariadb1

Traefik -->|client0.tetras-lab.io| nginx0
Traefik -->|client1.tetras-lab.io| nginx1

Utilisateur00 -->|https://client0.tetras-lab.io| Traefik
Utilisateur01 -->|https://client0.tetras-lab.io| Traefik
Utilisateur10 -->|https://client1.tetras-lab.io| Traefik
Utilisateur11 -->|https://client1.tetras-lab.io| Traefik
```

:::
:::

### Infrastructure applicative

:::{.columns}
:::{.column width=20%}

:::
:::{.column width=80%}

```{.mermaid}
graph LR

subgraph traefik
Traefik
end

subgraph nextcloud
nc_nginx
nc_db
nc_nextcloud
onlyoffice
end

subgraph ldap
openLdap
LAM[Ldap Account Manager]
end

subgraph gitlab
Gitlab
end

subgraph grafana
Grafana
end

Traefik -->|traefik|nc_nginx
Traefik -->|traefik|LAM
Traefik -->|traefik|Grafana
Traefik -->|traefik|Gitlab

nc_nginx -->|filesystem|nc_nextcloud
nc_nextcloud -->|default| nc_db
nc_nextcloud -->|default|onlyoffice

LAM -->|ldap|openLdap
Gitlab -->|ldap|openLdap
nc_nextcloud -->|ldap|openLdap

Grafana -->|ldap|openLdap
```

:::
:::

# Conclusion

### Dev Ops

\begin{figure}
\includegraphics[height=.45\textheight,keepaspectratio]{Devops-toolchain.png}
\caption{Crédit \href{https://fr.wikipedia.org/wiki/Fichier:Devops-toolchain.svg}{Kharnagy}}
\end{figure}

. . .

::: {.columns}
::: {.column width="48%"}

#### Objectifs

+ Augmenter la fiabilité
+ Faciliter le déploiement

:::
::: {.column width="48%"}

#### Principes

+ Pousser l'automatisation au maximum
+ Approche descriptive

:::
:::

### La conteneurisation en DevOps

* Facilite le déploiement
* Permet de lancer des environnements de tests complexe automatiquement
* Permet de décrire et versionner une architecture et des dépendances
* Permet de mettre en cohérence les configuration de plusieurs micro services
