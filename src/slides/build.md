---
title: Processus de compilation et de publication de conteneur
institute: IU2 MI4 Architecture Dev ops
author:
    - David Beniamine
date: 07/12/22
toc: true
toc-title: Sommaire
Licence: CC-BY-SA
aspectratio: 169
theme: Boadilla
header-includes: |
    \usepackage{textpos}
    \usepackage{booktabs}
    \usepackage[labelformat=empty]{caption}
    \useinnertheme[shadow]{rounded}
    \useoutertheme[subsection=false]{smoothbars}
    \definecolor{iutred}{HTML}{F2590D}
    \definecolor{keyword}{HTML}{007021}
    \hypersetup{
        colorlinks=true, %colorise les liens
        breaklinks=true, %permet le retour à la ligne dans les liens trop longs
        urlcolor= blue, %couleur des hyperliens
        linkcolor= iutred, %couleur des liens internes
        citecolor=blue,	 %couleur des liens de citations
        bookmarksopen=true,
        pdftoolbar=false,
        pdfmenubar=false,
    }
    \graphicspath{{img/}}
    \titlegraphic{\includegraphics[height=2cm]{logo.jpg}}
    \addtobeamertemplate{frametitle}{}{%
    \begin{textblock*}{90mm}(.85\textwidth,-.8cm)
        \includegraphics[width=2cm]{logo.jpg}
    \end{textblock*}}
    \usecolortheme[named=iutred]{structure}
    \usecolortheme{beaver}
    \usepackage{tikz}
    \usetikzlibrary{arrows, positioning, calc}
    \tikzset{%
      % Specifications for style of nodes:
                base/.style = {rectangle, rounded corners, draw=black,
                               minimum height=1cm,
                               text centered, font=\sffamily},
        host/.style = {base, fill=blue!30, minimum width=4.5cm},
        guest/.style = {base, minimum width=1cm, fill=green!30},
        mount/.style = {base, fill=orange!30},
        file/.style = {host, fill=orange!30},
        remote/.style = {base, fill=brown},
        hash/.style = {orange!80},
    }
    \AtBeginSubsection{}
    \makeatletter
    \setbeamertemplate{footline}{\hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1em}}
    \makeatother
---


# Introduction

### Interro flash

#### Interro
+ [RDV dans la section exercices sur Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php)
+ 5 questions
+ Temps limité : 5 minutes

### Retours sur le TP1



### Rappels conteneurs


#### Pincipe

+ Minimaliste
+ Isolation
+ Environnement contrôlé
+ Configurable par environnement

. . .

#### Utilité en Dev Ops

+ Approche micro services (vue en intro)
+ **Descriptif**
+ Orchestration **(on en parle au prochain cours)**

### Notion de recette

\begin{alertblock}{Approche Dev}
    \begin{itemize}
        \item Fournir un `Readme.md`
        \item Fournir un `Makefile` (ou autre)
        \item Lister les dépendances
    \end{itemize}
\end{alertblock}

. . .

\begin{block}{Approche Sys-admin}
    \begin{itemize}
        \item Configurer une Machine virtuelle aux petits oignons
        \item En faire un modèle ou une copie pour le futur
    \end{itemize}
\end{block}

. . .

\begin{exampleblock}{Approche DevOps}
    \begin{itemize}
        \item Transformer le `Readme.md` et le `Makefile` en fichier de recette
        \item Générer automatiquement une image
    \end{itemize}
\end{exampleblock}

# Compilation

## Recettes (Dockerfile)

### Construction du conteneur

\begin{center}
    \begin{tikzpicture}
        \node(hub)        [remote, text width=3cm] {Dépôt d'image\newline\url{hub.docker.com}};
        \node(Dockerfile) [file, text width=3cm, below left=2cm and .75cm of hub.north] {Recette (fichier)\newline commandes pour construire l'image};
        \uncover<2->{
            \node(image)      [host, text width=3cm, right=2cm of Dockerfile] {Fichiers du chroot\newline et meta données};
        }
        \uncover<3->{
            \node(cont1)      [guest, text width=3cm, below right=.5cm and .5cm of image] {Conteneur 1};
            \node(cont2)      [guest, text width=3cm, below=.5cm of cont1] {Conteneur 2};
        }

        \uncover<2->{
            \draw[thick, double, ->] (Dockerfile) -- node[below] {Compilation} (image);
            \draw[thick, double, ->] (hub) |- node[pos=.1, right] {Base} (image);
        }
        \uncover<3->{
            \draw[thick, double, ->] (image) |- node[pos=.7, above] {Instanciation} (cont1.west);
            \draw[thick, double, ->] (image) |- node[pos=.7, above] {Instanciation} (cont2.west);
        }
        \uncover<4->{
            \draw[thick, double, ->] (image) |- node[pos=.4, right] {Publication} (hub);
        }
    \end{tikzpicture}
\end{center}

### Hello word

::: {.columns}
::: {.column width="48%"}

#### `Dockerfile`
```Dockerfile
FROM debian:latest

RUN echo "Hello world" > /hello
```

:::
::: {.column width="48%"}

. . .

#### Fonctionnement
+ Image source
+ Commande arbitraire

:::
:::

. . .

#### Utilisation

```bash
# Compiler dans le dossier contenant le Dockerfile
docker build -t hello_world .
# Lancer
docker run --rm -it hello_world bash
```

### Processus de compilation

::: {.columns}
::: {.column width="60%"}

\begin{center}
    \begin{tikzpicture}
        \node (from) {\texttt{\textbf{\color{keyword}FROM} python:3.9}};
        \node (inst) [below=2cm of from.west, anchor=west] {\texttt{\textbf{\color{keyword}RUN} apt-get update \ \newline \&\& apt-get install -y graphviz}};
        \node (pip) [below=2cm of inst.west, anchor=west] {\texttt{\textbf{\color{keyword}RUN} pip install click\uncover<3->{\alert<3>{ flask}}}};
        \node (copy) [below=2cm of pip.west, anchor=west] {\texttt{\textbf{\color{keyword}COPY} entrypoint.sh /entrypoint}};
        \node (entrypoint) [below=2cm of copy.west, anchor=west] {\texttt{\textbf{\color{keyword}ENTRYPOINT}  /entrypoint}};

        \only<2->{
            \draw[hash,thick,->]   (from.south) -- node(c1)[hash,right,pos=.3] {\only<4->{\alert<4>{---> Using cache}}} node[hash,below=.1cm of c1.west, anchor=west] {---> a879e610c533} ($(from.south)-(0,1.5cm)$);
            \draw[hash,thick,->]   ($(from.south)-(0,2cm)$) -- node(c1)[hash,right,pos=.3] {\only<4->{\alert{---> Using cache}}} node[hash,below=.1cm of c1.west, anchor=west] {---> 3b30b532caab} ($(from.south)-(0,3.5cm)$);
            \draw[hash,thick,->]   ($(from.south)-(0,4cm)$) -- node(c1)[hash,right] {---> \only<-4>{66fdc4a3d6a3}\only<5->{\alert{73272582c172}}} ($(from.south)-(0,5.5cm)$);
            \draw[hash,thick,->]   ($(from.south)-(0,6cm)$) -- node(c1)[hash,right,pos=.3] {---> \only<-4>{29c4759af05d}\only<5->{\alert{01406c1f4da1}}} ($(from.south)-(0,6.5cm)$);
        }
    \end{tikzpicture}
\end{center}

:::
::: {.column width="38%"}

\only<2->{
\begin{block}{Construction par couche}
Une couche émise à chaque étape
\end{block}
}
\only<4->{
\begin{block}{Système de cache}
Réutilisation des couches intermédiaires
\end{block}
}

:::
:::

### Instructions de base

| Commande                                    | Exemple                                                     | Commentaire                                 |
|---------------------------------------------|-------------------------------------------------------------|---------------------------------------------|
| \texttt{\textbf{\color{keyword}FROM}}       | \texttt{\textbf{\color{keyword}FROM} debian:latest}         | Image de base                               |
| \texttt{\textbf{\color{keyword}ENV}}        | \texttt{\textbf{\color{keyword}ENV} PYTHONUNBUFFERED 1}     | Variable d'environement                     |
| \texttt{\textbf{\color{keyword}RUN}}        | \texttt{\textbf{\color{keyword}RUN} a2enmod rewrite}        | Commande arbitraire                         |
| \texttt{\textbf{\color{keyword}COPY}}       | \texttt{\textbf{\color{keyword}COPY} run /run}              | Ajout de fichier(s) au `chroot`             |
| \texttt{\textbf{\color{keyword}WORKDIR}}    | \texttt{\textbf{\color{keyword}WOKRDIR} /www}               | Changement de repertoir                     |
| \texttt{\textbf{\color{keyword}USER}}       | \texttt{\textbf{\color{keyword}USER} toto}                  | Changement d'utilisateur                    |
| \texttt{\textbf{\color{keyword}ENTRYPOINT}} | \texttt{\textbf{\color{keyword}ENTRYPOINT} ["/run", "foo"]} | Commande de lancement du conteneur          |
| \texttt{\textbf{\color{keyword}CMD}}        | \texttt{\textbf{\color{keyword}CMD} ["-v"]}                 | Ajout d'arguments par defaut a l'entrypoint |



### Compilation multi étapes

::: {.columns}
::: {.column width="48%"}

```Dockerfile
FROM debian:stable

RUN apt-get update && \
    apt-get install -y git \
        default-jdk \
        default-jre \
        maven
RUN git clone https://... app
WORKDIR /app
RUN mvn package

ENTRYPOINT java --jar /app/target/app.jar
```


:::
::: {.column width="48%"}

\begin{alertblock}{Problème}
    Certains languages demandent beaucoup de dépendances pour compiler.
\end{alertblock}

:::
:::

### Compilation multi étapes

::: {.columns}
::: {.column width="48%"}

```Dockerfile
FROM debian:stable as builder

RUN apt-get update && \
    apt-get install -y git \
        default-jdk \
        maven
RUN git clone https://... app
WORKDIR /app
RUN mvn package

FROM debian:stable
COPY --from-builder /app/target/app.jar /app.jar
RUN apt-get update && apt-get install -y default-jre
ENTRYPOINT java --jar /app.jar
```


:::
::: {.column width="48%"}

\begin{alertblock}{Problème}
    Certains languages demandent beaucoup de dépendances pour compiler.
\end{alertblock}

\begin{exampleblock}{Solution}
    Il est possible de créer des images intermédiaires dans le Dockerfile
\end{exampleblock}

:::
:::


# Publication

## Dépôt d'images

### Dépôts connus

#### Dockerhub

+ Le dépôt par défaut de docker
+ Vous l'avez déjà utilisé
+ Dépôt privé payant
+ **Hébergé par une entreprise tierce**

#### Gitlab

+ Hébergé sur n'importe quel instance gitlab
+ Lié à un dépôt
+ permet d'enregistrer des images avec le code correspondant
+ Voir [la documentation de gitlab](https://docs.gitlab.com/ce/user/packages/container_registry/)

### Petite appartée

#### Comment savoir si on peut faire confiance à un logiciel / entreprise ?

. . .

+ Y a-t-il un buisness model ?
    + Est-il raisonnable (prix ni trop haut ni trop bas) ?
    + Est-il clair et bien exposé ?
    + Qu'est ce qui est vendu (licences, support, "données") et à qui ?
+ Fonctionne-t-il par dons
    + Y a-t-il plusieurs "gros" donnateurs ?
    + Est-ce adapté au projet ?
+ Le code est-il accessible, si oui
    + Combien y a-t-il de contributeur·ices ?
    + Y a-t-il du travail récent ?
. . .
+ **êtes vous le client ou le produit ?**

. . . 

####
**Ces questions ne donnent pas de réponses absolues**


### Recommandations d'utilisation


::: {.columns}
::: {.column width="48%"}

\begin{exampleblock}{Utiliser les registry}
Ne pas réinventer la roue
\end{exampleblock}

\uncover<2->{
    \begin{alertblock}{Sécurité}
        \begin{itemize}
            \item Privilégier les images officielles
            \item Si non officiel \alert{\textbf{lire le Dockerfile}}
        \end{itemize}
    \end{alertblock}
}

\uncover<3->{
    \begin{block}{Maintenabilité}
        \begin{itemize}
            \item Privilégier les images maintenues
            \item Éviter les usines à gaz
            \item Choisir le bon tag
        \end{itemize}
    \end{block}
}

:::
::: {.column width="48%"}


\uncover<1->{
    \includegraphics[width=\textwidth]{dockerhub_bad.png}
}
\uncover<2->{
    \includegraphics[width=\textwidth]{dockerhub_official.png}
}
\uncover<3->{
    \includegraphics[width=\textwidth]{dockerhub_tag.png}
}

:::
:::

### Bonnes pratiques

#### Dans le Dockerfile

+ Finir par un `WORKDIR` sur le dossier principal de l'application
+ Faire un `USER` à la fin pour que l'entrypoint ne soit pas executé par `root`

#### Dans l'entrypoint

+ Installer les dépendances
+ Vider / regénérer les caches
+ Fixer les permissions



# TP

### Consignes globales

#### Organisation

+ On supposera que vous avez un dossier dédié pour le TP
* Mettre vos `Dockerfile` dans votre compte rendu

#### Documentation

[Référence dockerfile](https://docs.docker.com/engine/reference/builder)

## Instructions de bases

### Première image

1. Créer un premer sous dossier `etape1`
2. Créer un fichier `test` contenant le texte `Hello world`
3. Créer un fichier `Dockerfile`
4. Dans ce fichier, dans l'ordre
    * Utiliser l'image `debian` au tag `stable` comme base
    * Copier le fichier `test`
    * Installer le paquet vim
5. Compiler l'image (`docker build -t monimage .`)
    * À quoi sert l'option `-t monimage` ?
6. Lancer un conteneur utilisant cette image avec l'option `--rm`
7. Retrouver et modifier votre fichier `test`
8. Relancer un conteneur utilisant cette image, que contient le fichier `test` ?

### Recompilation

1. Dans le sous dossier `etape1`
2. Modifier le paquet à installer (`emacs` au lieu de `vim`) et recompiler
    * Quelles couches de la compilation précédente on été réutilisées ?
3. Modifier l'instruction de `COPY` (en rennomant votre fichier par exemple)
    * Quelles couches de la compilation précédente on été réutilisées ?

### Entrypoint

1. Dans un sous dossier `etape2`
2. Faire un `Dockerfile` qui installe le paquet `netcat`
3. Déclarer comme entrypoint la commande `nc -l -p 1337`
    * Que fait cette commande ?
4. Compiler l'image et lancer un conteneur en "bindant" le port 1337 en local
5. Sur votre pc lancer `nc localhost 1337` que ce passe-t-il ?
6. Modifier votre `Dockerfile` afin d'utiliser la variable d'environement `PORT` plutôt que le `1337` arbitraire
    * Attention la commande `ENTRYPOINT` ne gère pas les variables d'environement, il faudra faire un script
7. Recompiler, relancer un conteneur et utiliser la variable d'environement pour changer le port
    * Que se passe-t-il si vous ne définissez pas la variable d'environnement `PORT` au lancement de votre conteneur ?

## Options avancées

### Volumes

1. Dans un sous dossier `etape3` créer un `Dockerfile`
2. Utiliser l'instruction `VOLUME` sur un dossier `/data` par exemple
3. Créer un fichier de test dans ce dossier
4. Compiler votre image et lancer un conteneur
5. Est-ce que vous trouvez votre fichier ?
    * Pourquoi ?
    * Quel est l'interet de cette instruction ?
6. Peut-on utiliser l'option `-v` de `docker run` sur un dossier qui n'est pas déclaré comme un volume dans le `Dockerfile` ?

### Ports

1. Dans un sous dossier `etape4` reprendre le `Dockerfile` de l'étape2
2. Ajouter l'instruction `EXPOSE 1337`
3. Que fait cette instruction ?
4. Est-ce qu'elle vous dispense de l'option `-p` dans la commande `docker run` ?
5. Quel est l'intérêt de cette instruction ?

### Choix d'une image de base

* Quelle image de base et avec quel tag utiliseriez vous pour les cas d'usage suivant et pourquoi ?
    1. Un serveur de base de donnée SQL
    2. Un serveur web
    3. Une application python
    4. Un service utilisant uniquement des scripts fait par vos soins avec des commandes standard

## Compilation multi étapes

### Inspecter les images créees

1. La ligne de commande `docker image` permet de lister les images existante sur votre PC
2. Est-ce que les couches intermédiaires apparaissent ?
3. Comment les afficher ?
4. Modifier un de vos `Dockerfile` et comptez le nombre d'images avant et après (par exemple avec un `wc -l`, est-ce cohérent ?
5. Comment supprimer les images inutilisées ?

### Réaliser une compilation à plusieurs étapes

1. Créer un `Dockerfile`
2. Utilisez l'instruction `FROM debian:stable as builder`, installer `default-jdk` et compiler un [hello world](https://rosettacode.org/wiki/Hello_world/Text#Java)
3. Plus bas ajouter l'instruction `FROM debian:stable`, installer `default-jre` et lancer votre hello world en entrypoint
4. Est ce que `javac` est-il disponible dans votre image finale ?
5. Modifier votre `Dockerfile` pour mettre en commun l'installation du jre (vous pouvez utiliser un `FROM` de plus)

### Bonus

1. Reprendre la question bonus de la semaine dernière et faire un `Dockerfile` intégrant votre script à l'image de compilation
    + Il est possible d'utiliser la correction
2. Votre script ne permet pas de modifier les arguments de compilation.
    1. Utiliser une variable d'environnement afin de permettre à l'utilisateur d'ajouter des arguments
    2. Utiliser un `Makefile`
        + Dans votre script lancer `make` au lieu de `pandoc`
        + Installer les paquets nécessaire dans votre image
        + Modifier le dossier `www` pour mettre un `Makefile` produisant les `.html`

# Conclusion

### Rendu

+ CR due sur [Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php) section travaux avant ce soir 23h59
+ N'hésitez pas à écrire sur le CR si des points ne vous semblent pas clairs
+ Les comptes rendus doivent être clair et lisibles
+ Je ne dois pas avoir besoin de relire l'énnoncé pour vous comprendre

### Conclusion

+ Utilisation de recette (approche descriptive)
+ Compilation par étape
    * Réutilisation de caches / couches communes
    * Garder les conteneur minimalistes (approche micro service)

. . .

#### Pour aller plus loin

+ `ARGS` : permet de définir des variables lors de la compilation
+ `ENV` : pour déclarer la valeur par défaut de variables d'environnement
+ Processus de publication d'une image

### Orchestration

#### Objectifs 
* Organiser plusieurs conteneurs ensemble
* Mettre l'environnement en cohérence
* Gérer en groupes les services

####
**On en parle au prochain cours**

### Bonnes fêtes !

On se revoit en 2023, alors profitez des vacances

. . .

**Mais n'oubliez pas qu'il y aura une intérro au retour ;)**
