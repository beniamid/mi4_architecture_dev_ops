---
title: Projet conteneurs
institute: IU2 MI4 Architecture Dev ops
author:
    - David Beniamine
date: 30/01/23
toc: true
toc-title: Sommaire
Licence: CC-BY-SA
aspectratio: 169
theme: Boadilla
header-includes: |
    \usepackage{textpos}
    \usepackage{booktabs}
    \usepackage[labelformat=empty]{caption}
    \useinnertheme[shadow]{rounded}
    \useoutertheme[subsection=false]{smoothbars}
    \definecolor{iutred}{HTML}{F2590D}
    \definecolor{keyword}{HTML}{007021}
    \hypersetup{
        colorlinks=true, %colorise les liens
        breaklinks=true, %permet le retour à la ligne dans les liens trop longs
        urlcolor= blue, %couleur des hyperliens
        linkcolor= iutred, %couleur des liens internes
        citecolor=blue,	 %couleur des liens de citations
        bookmarksopen=true,
        pdftoolbar=false,
        pdfmenubar=false,
    }
    \graphicspath{{img/}}
    \titlegraphic{\includegraphics[height=2cm]{logo.jpg}}
    \addtobeamertemplate{frametitle}{}{%
    \begin{textblock*}{90mm}(.85\textwidth,-.8cm)
        \includegraphics[width=2cm]{logo.jpg}
    \end{textblock*}}
    \usecolortheme[named=iutred]{structure}
    \usecolortheme{beaver}
    \usepackage{tikz}
    \usetikzlibrary{arrows, positioning, calc}
    \tikzset{%
      % Specifications for style of nodes:
                base/.style = {rectangle, rounded corners, draw=black,
                               minimum height=1cm,
                               text centered, font=\sffamily},
        host/.style = {base, fill=blue!30, minimum width=4.5cm},
        guest/.style = {base, minimum width=1cm, fill=green!30},
        mount/.style = {base, fill=orange!30},
        file/.style = {host, fill=orange!30},
        remote/.style = {base, fill=brown},
        hash/.style = {orange!80},
    }
    \AtBeginSubsection{}
    \makeatletter
    \setbeamertemplate{footline}{\hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1em}}
    \makeatother
---

# Projet

### Interro flash

#### Interro
+ [RDV dans la section exercices sur Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php)
+ 5 questions
+ Temps limité : 5 minutes

### Objectif

* Mettre en place un déploiement complet de [Leed RSS](https://github.com/LeedRSS/Leed)
* Aucune autre confiuration nécessaire que le `.env`
* Documenter ce qui est nécessaire / ce qui est possible
* Faire une démonstration de ce que vous avez appris sur Docker

### Organisation

#### Temporelle
* Travail jusqu'à 15h30
* Rendu
* 1h évaluation par les pairs et discussions

#### Rendu

+ Travail par binôme
+ Dépôt git + hash du dernier commit = 1 url

### Consignes

1. Au moins 2 services obligatoires **et 1 optionnel**
2. Au moins un des services utilise une image sur mesure `Dockerfile`
3. Au moins un port exposable pour accéder à l'application
   * **Le port doit être changeable**
4. Au moins un volume mis en place pour persistance des données
5. Présence d'au moins une variable d'environement pour changer un comportement de l'application
6. Mode production et développement séparé
   + Ex : xdebug pour le dev - accès facile aux sources
7. Procédure de nettoyage au démarrage (cache, modification locales de sources, ...)
8. Le readme doit expliquer la configuration minimale **et les modifications possible**

#### Consignes obligatoires

**Les consignes 1 à 4 (incluse) sont aubligatoire, leur non respect passe votre rendu en hors sujet (6/20 maximum)**
