---
title: Introduction à la conteneurisation
institute: IU2 MI4 Architecture Dev ops
author:
    - David Beniamine
date: 06/12/22
toc: true
toc-title: Sommaire
Licence: CC-BY-SA
aspectratio: 169
theme: Boadilla
header-includes: |
    \usepackage{textpos}
    \usepackage{booktabs}
    \usepackage[labelformat=empty]{caption}
    \useinnertheme[shadow]{rounded}
    \useoutertheme[subsection=false]{smoothbars}
    \definecolor{iutred}{HTML}{F2590D}
    \hypersetup{
        colorlinks=true, %colorise les liens
        breaklinks=true, %permet le retour à la ligne dans les liens trop longs
        urlcolor= blue, %couleur des hyperliens
        linkcolor= iutred, %couleur des liens internes
        citecolor=blue,	 %couleur des liens de citations
        bookmarksopen=true,
        pdftoolbar=false,
        pdfmenubar=false,
    }
    \graphicspath{{img/}}
    \titlegraphic{\includegraphics[height=2cm]{logo.jpg}}
    \addtobeamertemplate{frametitle}{}{%
    \begin{textblock*}{90mm}(.85\textwidth,-.8cm)
        \includegraphics[width=2cm]{logo.jpg}
    \end{textblock*}}
    \usecolortheme[named=iutred]{structure}
    \usecolortheme{beaver}
    \usepackage{tikz}
    \usetikzlibrary{arrows, positioning, calc}
    \tikzset{%
      % Specifications for style of nodes:
                base/.style = {rectangle, rounded corners, draw=black,
                               minimum height=1cm,
                               text centered, font=\sffamily},
        host/.style = {base, fill=blue!30, minimum width=4.5cm},
        guest/.style = {base, minimum width=1cm, fill=green!30},
        mount/.style = {base, fill=orange!30},
        file/.style = {host, fill=orange!30},
        remote/.style = {base, fill=brown},
    }
    \AtBeginSubsection{}
    \makeatletter
    \setbeamertemplate{footline}{\hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1em}}
    \makeatother
---



# Introduction

### Dev Ops

\begin{figure}
\includegraphics[height=.45\textheight,keepaspectratio]{Devops-toolchain.png}
\caption{Crédit \href{https://fr.wikipedia.org/wiki/Fichier:Devops-toolchain.svg}{Kharnagy}}
\end{figure}

. . .

::: {.columns}
::: {.column width="48%"}

#### Objectifs

+ Augmenter la fiabilité
+ Faciliter le déploiement

:::
::: {.column width="48%"}

#### Principes

+ Pousser l'automatisation au maximum
+ Approche descriptive

:::
:::


### Le DevOps et moi

#### Mon métier

+ Co-gérant d'une TPE
    + Responsable administratif
    + Développeur "full-stack"
    + Administrateur système / support

. . .

#### Enjeux

+ Facilité / homogénéité d'administration
+ Fiabilité
+ Reproductibilité
+ Cohérence sur le lancement de mes projets

. . .

####
On parlera des mes différentes utilisations des technologies présentées ici en ouverture

### UE de DevOps

::: {.columns}
::: {.column width="48%"}

#### MI4 devOps **[David Beniamine](mailto:David.Beniamine@univ-grenoble-alpes.fr)**

+ **Conteneurs et micro-services**
+ **Déploiement descriptif**
+ **Orchestration de Conteneurs**

:::
::: {.column width="48%"}

#### MI3 tests et qualité [Millian Poquet](mailto:Millian.Poquet@inria.fr)

+ Gestion de version
+ Tests automatiques
+ Qualité de code

:::
:::

. . .

#### Ressources

+ [Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php)
+ [Ma page (slides et infos)](https://beniamid.gricad-pages.univ-grenoble-alpes.fr/mi4_architecture_dev_ops/)

### Évaluation

#### 

+ 3 intéros de début de cours (5min)
    + 07/12/22
    + 04/01/23
    + 30/01/23
+ 1 mini projet
    + Évalué sur la facilité de déploiement
+ 3 CR de TP à rendre le soir même


### Organisation temporelle

#### Interro de début de cours

+ 5 min, commence pile à l'ouverture du cours
+ Retard = temps perdu

. . .

+ **Intérro maintenant** Allez sur chamillo maintenant !

. . . 

#### Présentation puis TP

+ Questions dans les slides
+ Compte rendus :
    + Par bi/trinôme
    + À déposer sur [Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php)
    + **évalué : rendu avant minuit le lendemain du TP**

. . .

#### Attention

+ Les énoncés donnent souvent des indications sur les questions précédentes
+ Prenez le temps de tester avant d'avancer

### Programme de l'UE

+ **05/12/22 3h**
    + Introduction au conteneurs et découverte de la CLI `docker`
+ **07/12/22 3h**
    + Processus de compilation et de publication de conteneur introduction au `Dockefile`
+ **04/01/23 6h**
    + Orchestration de conteneur avec `docker-compose`
    + Ouverture
+ **30/12/21 6h**
    + Mini projet
    + Evaluation par les pairs et discussions


# Conteneurisation contre virtualisation

## Intro

### Machines virtuelles


\begin{exampleblock}{The good (avantages)}
    \begin{itemize}
        \item Isolation complète (réseau, noyau, système de fichier)
        \item Possibilité de simuler du matériel
        \item Sauvegardes
        \item Gestion des ressources
    \end{itemize}
\end{exampleblock}


. . .

\begin{block}{The bad (limites)}
    \begin{itemize}
        \item Lourd à partager (image disques > 5Go)
        \item Gestion des disques
        \item Administration système
    \end{itemize}
\end{block}


. . .

\begin{alertblock}{The ugly (presque impossible)}
    \begin{itemize}
        \item Réinitialisation sans pertes de données
        \item Comprendre comment une VM à été conçue / modifiée
    \end{itemize}
\end{alertblock}


### Alléger la virtualisation

::: {.columns}
::: {.column width="48%"}

\begin{center}
    \begin{tikzpicture}
      \node(host)   [host]                              {Hôte};
      \node(vbox) [host, above=.5cm of host]               {Gestionnaire de VM};
      \node(guest1)   [guest, above right=.5cm and 0.2cm of vbox.north west, anchor=south west] {OS1};
      \node(guest2) [guest, right=.5cm of guest1] {OS2};
      \node(guest3) [guest, right=.5cm of guest2] {OS3};

      \node(app1)   [guest, above=.5 cm of guest1] {App2};
      \node(app2)   [guest, above=.5 cm of guest2] {App2};
      \node(app3)   [guest, above=.5 cm of guest3] {App3};

      \node(vm1)   [above=.5 cm of app1] {VM1};
      \node(vm2)   [above=.5 cm of app2] {VM2};
      \node(vm3)   [above=.5 cm of app3] {VM3};

      \draw ($(guest1.south east) + (.1cm, -0.1cm)$) rectangle ($(vm1.north west) - (.1cm,0)$);
      \draw ($(guest2.south east) + (.1cm, -0.1cm)$) rectangle ($(vm2.north west) - (.1cm,0)$);
      \draw ($(guest3.south east) + (.1cm, -0.1cm)$) rectangle ($(vm3.north west) - (.1cm,0)$);
    \end{tikzpicture}
\end{center}

:::
::: {.column width="48%"}

. . .

\begin{center}
    \begin{tikzpicture}
      \node(host)   [host]                              {Hôte};
      \node(docker) [host, above=.5cm of host]               {Gestionnaire de conteneur};
      \node(app1)   [guest, above=.5cm and 0cm of docker.north west, anchor=south west] {App1};
      \node(app2)   [guest, right=.5 cm of app1] {App2};
      \node(app3)   [guest, right=.5 cm of app2] {App3};
      \node(align)  [above=2.3cm of app1] {}; % To align with vm image

      \draw ($(app1.south east) + (.1cm, -0.1cm)$) rectangle ($(app1.north west) - (.1cm,-.5cm)$);
      \draw ($(app2.south east) + (.1cm, -0.1cm)$) rectangle ($(app2.north west) - (.1cm,-.5cm)$);
      \draw ($(app3.south east) + (.1cm, -0.1cm)$) rectangle ($(app3.north west) - (.1cm,-.5cm)$);
    \end{tikzpicture}
\end{center}

:::
:::

## Quelques notions de système d'exploitation

### Qu'est ce qu'une Machine virtuelle ?

+ Un gestionnaire (KVM / Qemu, Virtualbox, Vmware)

. . .

+ Qui lance des machines virtuelles

. . .

+ C'est à dire des Systèmes d'exploitation

. . .

+ Avec une vue partielle du matériel (espace disque, nombre et nature des processeurs, de la mémoire etc.)

### Qu'est ce qu'un système d'exploitation ?

. . .

```{.mermaid width=800 caption="Représentation simplifiée"}

flowchart TD
    Mat[Matériel]
    P[Périphériques]

    subgraph OS[Système d'exploitation]
        subgraph N[Noyau]
            O[Ordonnanceur]
            Mem[Abstraction mémoire]
            ES[Entrées / Sorties]
            F[Système de fichier]
            R[Réseau]
        end
        subgraph HL[ ]
            S[Shell]
            B[Bibliothèques]
            T[Outils de base]
        end
    end

    App[Applications]
    U[Utilisateur]

    U <--> App
    HL <--> N
    App <--> HL
    N <--> Mat
    N <--> P
```

### Comment alléger la virtualisation ?

. . .

+ La plupart du temps le matériel est identique entre l'hôte et la VM

. . .

+ Ne pas dupliquer le noyau de l'OS

. . .

+ Ajouter le minimum de bibliothèques et d'outils

. . . 

#### Comment isoler ?

. . . 

Connaissez vous le Chroot ?


### Chroot : Change Root

```
/bin
  bash
  ls
  foo
/lib
/etc
/usr
/target
    /jail  # chroot /target/jail empêche le shell de remonter plus haut
        /bin
            bash
        /lib
```

### Utilité d'un chroot

+ Dépannage
+ Test
+ Cloisonnement (execution de code supicieux)

. . .

#### Demo
https://gitlab.tetras-libre.fr/dbeniamine/chroot-demo

## Les conteneurs

### Comment fonctionne un conteneur ?

\begin{center}
    \begin{tikzpicture}
      \node(kern)  [host, minimum width=5.5cm]                              {Noyau};
      \uncover<4->{
        \node(hnet)  [mount, above=.5cm and 0cm of kern.north west, anchor=south west] {Réseau};
      }
      \uncover<5->{
        \node(hvol)  [mount, above=.5cm and 0cm of kern.north east, anchor=south east] {Système de fichiers};
      }
      \uncover<3->{
        \node(bin)   [guest, above=.5cm and 0cm of hnet.north west, anchor=south west] {Binaires};
        \node(libs)  [guest, above=.5cm and 0cm of hvol.north east, anchor=south east] {Bibliothèques};
        \uncover<4->{
            \node(net)   [mount, above=.5cm and 0cm of bin.north west, anchor=south west] {Réseau};
        }
        \uncover<5->{
            \node(vol)   [mount, above=.5cm and 0cm of libs.north east, anchor=south east] {Volumes};
        }
        \node(app)   [guest, above=.5cm and 0cm of net.north west, anchor=south west, minimum width=5.5cm] {Application};
      }
      \node(docker) [host, left=.5cm of kern.south west, anchor=south west, rotate=90]               {Gestionnaire de conteneur};


      \uncover<2->{
        \node(container) [right=.5cm of libs.south east, rotate=90, anchor=north west] {Conteneur (chroot)};
        \draw ($(bin.south west) - (.1cm, .1cm)$) rectangle ($(app.north east) + (1.1cm,.1cm)$);
      }


      \node(host) [right=.5cm of kern.south east, rotate=90, anchor=north west] {Hôte};
      \draw ($(kern.south west) - (.1cm, .1cm)$) rectangle ($(hvol.north east) + (1.1cm,.1cm)$);

      \uncover<5->{
        \draw[<-,color=orange, thick] (vol.north) -- ($(vol.north)+(0,.3cm)$) -| (docker.east);
        \draw[color=orange, thick]   ($(docker.south)-(0,1.05cm)$) |- ($(hvol.south)-(0,.3cm)$) -| (hvol.south);
      }


      \uncover<4->{
        \draw[thick, blue] (net.south) -- ($(net.south)-(0,.3cm)$) -| ($(docker.south)+(0,2cm)$);
        \draw[thick, blue, ->]   ($(docker.south)+(0,.55cm)$) |- ($(hnet.north)+(0,.3cm)$) -| (hnet.north);
      }
    \end{tikzpicture}
\end{center}

### Modèle d'isolation et de communication

#### Isolation

+ Noyau partagé avec l'hôte
+ `chroot` ne contenant que les fichiers nécessaires
+ Couche réseau dédiée
+ Système de fichier en couches (`layer`)
+ Définition de réseaux

. . .

#### Communications
+ Utilisation de volumes (montage de fichiers)
+ Port "mapping"


### Construction du conteneur

\begin{center}
    \begin{tikzpicture}
        \node(hub)        [remote, text width=3cm] {Dépot d'image\newline\url{hub.docker.com}};
        \node(Dockerfile) [file, text width=3cm, below left=2cm and .75cm of hub.north] {Recette (fichier)\newline commandes pour construire l'image};
        \uncover<2->{
            \node(image)      [host, text width=3cm, right=2cm of Dockerfile] {Fichiers du chroot\newline et meta données};
        }
        \uncover<3->{
            \node(cont1)      [guest, text width=3cm, below right=.5cm and .5cm of image] {Conteneur 1};
            \node(cont2)      [guest, text width=3cm, below=.5cm of cont1] {Conteneur 2};
        }

        \uncover<2->{
            \draw[thick, double, ->] (Dockerfile) -- node[below] {Compilation} (image);
            \draw[thick, double, ->] (hub) |- node[pos=.1, right] {Base} (image);
        }
        \uncover<3->{
            \draw[thick, double, ->] (image) |- node[pos=.7, above] {Instanciation} (cont1.west);
            \draw[thick, double, ->] (image) |- node[pos=.7, above] {Instanciation} (cont2.west);
        }
        \uncover<4->{
            \draw[thick, double, ->] (image) |- node[pos=.4, right] {Publication} (hub);
        }
    \end{tikzpicture}
\end{center}

\uncover<5->{
    \begin{block}{}
        Ce processus sera étudié en détails dans le prochain cours
    \end{block}
}

## En pratique

### Principes et utilisation

#### Micro services
+ Conteneurs minimalistes (1 conteneur = 1 service)
+ Exposer uniquement le nécessaire
+ Utiliser des volumes pour les données **devant** être persistantes

. . .

#### Mise à jour / Réinitialisation

+ Supprimer le conteneur récupère les fichiers hors volumes
+ Il suffit de mettre à jour l'image de base et recréer un conteneur pour mettre à jour

. . .

#### Configuration
+ **Variables d'environnement**
+ Montage de dossier de configuration


### Technologies de conteneurs

#### LXC
+ [linuxcontainers.org](https://linuxcontainers.org/)
+ Linux (intégré au noyau)
+ Libre (GPL)
+ Bas niveau

. . .

#### Docker
+ [docker.com](https://www.docker.com/)
+ Entreprise privée
+ Code libre (Apache v2)
+ Utilise LXC
+ Multi plateformes (ou presque)

# TP

## Intro

###  Préparation

#### Installer docker
+ Linux (Debian / Ubuntu) :
```bash
apt-get install docker.io
```
+ [Autres OS](https://docs.docker.com/engine/install/)

#### Consigne
**Utiliser uniquement la ligne de commande et non pas l'application docker desktop durant le TP**

### Lancer un premier container

1. Lancer la commande
```bash
docker run --rm -it debian bash
```
2. Explorer le conteneur chercher ce qui est installé dessus.
3. Installer `vim` (`apt-get install vim`)
4. Lancer `vim` (`:q!` pour quitter)
5. Dans un autre terminal lancer `docker ps`
6. Quitter votre premier container et relancer
```bash
docker run --rm -it debian bash
```
7. Lancer `vim` que ce passe-t-il  et pourquoi ?
8. Lancer `docker ps`
9. Est-ce que cela confirme votre réponse à la question précédente ?
10. Que fait chaque paramètre de la première commande (voir [la documentation](https://docs.docker.com/engine/reference/commandline/cli/))?

## Mécanismes de base

### Isolation

1. Garder le conteneur de l'étape précédente ou le relancer au besoin
2. Dans un second terminal lancer à nouveau
```bash
docker run --rm -it debian bash
```
2. Dans chacun des conteneurs installer `ping` et `ip` : `apt update && apt install iproute2 inetutils-ping`
4. Dans chacun des conteneurs faire `ip a` pour trouver votre IP
5. Essayer de "pinger" le conteneur voisin, est-ce que cela fonctionne ?
6. Fermer un des deux conteneurs
7. Créer un réseau et relancer un nouveau conteneur
```bash
docker network create --driver bridge test
docker run --rm -it --network test debian bash
```
8. Ressayer de "pinger" l'autre conteneur
9. Que se passe-t-il / qu'en concluez vous ?

### Volumes

1. Lancer
```bash
docker run -v "$PWD/toto:/home/toto" --rm -it debian bash
```
2. Dans le conteneur : `echo "Hello world" > /home/toto/test`
3. Regarder le contenu du dossier toto sur l'hôte
4. Quitter le conteneur et en relancer un nouveau avec la même commande
5. Le fichier est-il toujours là, pourquoi (vous pouvez utiliser `cat /home/toto/test`) ?

### Conteneur nommé

1. Lancer
```bash
docker run --name apache php:apache
```
2. Que fait cette commande ?
3. Peut voir les fichiers servit par apache, depuis l'hôte ?
4. Comment arrêter / relancer / supprimer ce conteneur ?
5. comment faire ces opérations avec un conteneur non nommé ?


### Ports

1. Arrêter et supprimer le conteneur de l'étape précédente
```bash
docker stop apache
docker rm apache
```
2. Relancer le conteneur avec une redirection de ports
```bash
docker run --name apache -p 8000:80 php:apache
```
3. Aller sur [http://localhost:8000](http://localhost:8000)
4. Essayer de modifier ce qui est affiché (`docker exec -it apache bash` pour ouvrir un terminal dans ce container)

. . .

#### Correction

4. Une fois dans un terminal sur le conteneur, `echo "Bonjour" > index.html`

**Note** cette fois nul besoin de conaitre l'IP du conteneur

## Configuration et exemple complet

### Variable d'environnements

1. Lancer :
```bash
docker run -e TOTO=salut --rm -it debian bash
```
2. Lancer la commande `echo $TOTO`
3. Lancer la commande `env`
4. Passer plusieurs fois `-e` avec des variables différentes à docker, regarder ce que cela fait

### Mini site

Nous allons désormais utiliser docker pour servir un site web en php qui affiche `bonjour <nom>`
où nom est une variable d'environnement passée au lancement.

Trouver la ou les commandes docker qui permettent d'afficher ce site

Voici un fichier php qui fait l'affichage:

#### `index.php`
```{.PHP}
Bonjour
<?php
echo $_ENV['NOM'];
```

### Bonus

1. Écrire un script qui convertit tous les fichiers markdown d'un dossier en html.
    + Ce script ne fait rien si il existe déjà un fichier html plus récente que sa source
    + Ce script tourne à l'infini
2. Lancer un conteneur qui execute ce script
3. Lancer un conteneur qui sert la page web générée par le premier conteneur

# Conclusion

### Rendu

+ CR due sur [Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php) section travaux avant ce soir 23h59
+ N'hésitez pas à écrire sur le CR si des points ne vous semblent pas clairs
+ Les comptes rendus doivent être clair et lisibles
+ Je ne dois pas avoir besoin de relire l'énnoncé pour vous comprendre

### Commandes de base docker

```bash
# Lancer un conteneur "1 shot" et ouvrir un terminal dedans
docker run --rm -it <image> bash
# Lancer un conteneur nommé
docker run --name <nom> <image>
# Ouvrir un terminal dans un conteneur
docker exec -it <nom> bash
# Lancer, stopper, supprimer un conteneur nomme
docker [start|stop|rm] <nom>
# Voir les contemneur
docker ps -a
# Voir les images, volumes, réseaux
docker [image|volume|network] ls
```


### Introduction à la contenerisation

#### Conteneurs

+ Minimaliste
+ Isolation
+ Environnement contrôlé
+ Réinitialisable
+ Configurable via des variables d'environnement

. . .

#### Utilité en Dev Ops

+ Approche micro services
+ Descriptif **(cours sur la compilation)**
+ Orchestration **(cours sur l'orchestration)**

. . .

#### Limites

Noyau partagé avec l'hôte
