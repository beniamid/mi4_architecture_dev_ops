---
title: Orchestration de conteneurs
institute: IU2 MI4 Architecture Dev ops
author:
    - David Beniamine
date: 04/01/23
toc: true
toc-title: Sommaire
Licence: CC-BY-SA
aspectratio: 169
theme: Boadilla
header-includes: |
    \usepackage{textpos}
    \usepackage{booktabs}
    \usepackage[labelformat=empty]{caption}
    \useinnertheme[shadow]{rounded}
    \useoutertheme[subsection=false]{smoothbars}
    \definecolor{iutred}{HTML}{F2590D}
    \definecolor{keyword}{HTML}{007021}
    \hypersetup{
        colorlinks=true, %colorise les liens
        breaklinks=true, %permet le retour à la ligne dans les liens trop longs
        urlcolor= blue, %couleur des hyperliens
        linkcolor= iutred, %couleur des liens internes
        citecolor=blue,	 %couleur des liens de citations
        bookmarksopen=true,
        pdftoolbar=false,
        pdfmenubar=false,
    }
    \graphicspath{{img/}}
    \titlegraphic{\includegraphics[height=2cm]{logo.jpg}}
    \addtobeamertemplate{frametitle}{}{%
    \begin{textblock*}{90mm}(.85\textwidth,-.8cm)
        \includegraphics[width=2cm]{logo.jpg}
    \end{textblock*}}
    \usecolortheme[named=iutred]{structure}
    \usecolortheme{beaver}
    \usepackage{tikz}
    \usetikzlibrary{arrows, positioning, calc}
    \tikzset{%
      % Specifications for style of nodes:
                base/.style = {rectangle, rounded corners, draw=black,
                               minimum height=1cm,
                               text centered, font=\sffamily},
        host/.style = {base, fill=blue!30, minimum width=4.5cm},
        guest/.style = {base, minimum width=1cm, fill=green!30},
        mount/.style = {base, fill=orange!30},
        file/.style = {host, fill=orange!30},
        remote/.style = {base, fill=brown},
        hash/.style = {orange!80},
    }
    \AtBeginSubsection{}
    \makeatletter
    \setbeamertemplate{footline}{\hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1em}}
    \makeatother
---

# Introduction

### Bonne année

####
Bonne année 2023 !

### Interro flash

#### Interro
+ [RDV dans la section exercices sur Chamilo](https://chamilo.iut2.univ-grenoble-alpes.fr/courses/INFOMESSIADMIN3/index.php)
+ 5 questions
+ Temps limité : 5 minutes

### Rappels conteneurs

#### Pincipe

+ Minimaliste
+ Isolation
+ Environnement contrôlé
+ Configurable par environnement

. . .

#### Utilité en Dev Ops

+ Approche micro services (vue en intro)
+ Descriptif (vue pendant le cours sur la compilation)
+ **Orchestration**

### Cycle de vie des conteneurs

#### Ce qu'on a vu
1. Création d'image à partir de `Dockerfile`
2. Compilation avec `docker build`
3. Instanciation avec `docker run`

. . .

#### Limites
+ Comment retrouver exactement la commande utilisée  pour instancier un conteneur ?
+ Comment s'assurer que deux conteneurs partagent les mêmes variables d'environnements ?
+ Comment isoler un groupe de conteneur dans un réseau ?
+ Comment mettre ça dans un dépôt `git` (ou autre) ?

### `docker-compose.yml`

####
> One file to rule them all,  
> One file to find them,  
> One file to bring them all  
> and in the network bind them.


# Orchestration

### Qu'est ce que `docker-compose` ?


#### `docker-compose.yml`
+ Fichier `yaml`
+ Décrit
    + Des services
    + Des réseaux
    + Des volumes
    + Leurs interactions

#### `docker-compose`
La ligne de commande pour interagir avec les conteneurs du `docker-compose.yml`

### Exemple minimaliste


::: {.columns}
::: {.column width="48%"}


#### `docker-compose.yml`
```yaml
version: "3"

services:
  front:
    image: php:apache
```


:::
::: {.column width="48%"}

. . .

#### Configuration
+ Ports
+ Variables d'environement
+ Réseau(x)
+ Volumes
+ Compilation

. . .

:::
:::

#### Avantages
* Un fichier décrit l'architecture
* Facile à mettre dans un dépôt `git`
* Assure la cohérence entre les services
* Fait le lien entre l'image et le déploiement

## Configuration

### Variables d'environnement

::: {.columns}
::: {.column width="48%"}


#### `docker-compose.yml`
```yaml
version: "3"

services:
  front:
    image: php:apache
    environment:
        - FIRST_NAME=David
        - LAST_NAME=Beniamine
```

. . .

:::
::: {.column width="48%"}

. . .

#### Limites
* Configuration potentiellement privées dans le fichier
* Le `docker-compose.yml` n'est pas générique

:::
:::

. . .

####
Comment séparer les variables de leur utilisation ?

### Utilisation du fichier `.env`

::: {.columns}
::: {.column width="48%"}


#### `docker-compose.yml`
```yaml
version: "3"

services:
  front:
    image: php:apache
    environment:
        - FIRST_NAME=${FIRST_NAME}
        - LAST_NAME=${LAST_NAME}
```

. . .

:::
::: {.column width="48%"}

#### `.env`
```bash
FIRST_NAME=David
LAST_NAME=Beniamine
```

. . .

#### Sucre syntaxique

```yaml
environment:
    FIRST_NAME:
    LAST_NAME:
```

Donne la valeur de la variable depuis le `.env`

:::
:::

. . .

#### Recommandations git
* Ignorer le `.env`
* Ajouter un `.env.sample` avec les valeurs par défaut


### Ports

::: {.columns}
::: {.column width="48%"}


#### `docker-compose.yml`
```yaml
version: "3"

services:
  front:
    image: php:apache
    ports:
      - 8001:80
```


:::
::: {.column width="48%"}

. . .

####
+ Bind le port `8001` de l'hote sur le port `80` du conteneur `front`
+ `${APACHE_PORT}:80` pour utiliser une variable d'environnement
* Autant de port mapping que l'on veut

:::
:::

### Volumes

::: {.columns}
::: {.column width="48%"}

#### Volume mapping
* Monte un dossier de l'hôte sur le conteneur
* Chemin relatif au `docker-compose.yml`

#### `docker-compose.yml`
```yaml
version: "3"

services:
  front:
    image: php:apache
    volumes:
      - ./www:/var/www/html
```

:::
::: {.column width="48%"}

. . .

#### Volume nommé
* Fait persister un dossier du conteneur
* Pas de bind dans le repertoire courant

#### `docker-compose.yml`
```yaml
version: "3"
services:
  front:
    image: php:apache
    volumes:
      - www:/var/www/html
volumes:
  www:
```

:::
:::

### Réseaux

::: {.columns}
::: {.column width="48%"}

#### `docker-compose.yml`
```yaml
version: "3"
services:
  front:
    image: php:apache
    networks:
      - front
      - default
      - db
  cache:
      image: memcached
```

:::
::: {.column width="48%"}

#### `docker-compose.yml` (suite)
```yaml
  db:
    image: mariadb
    networks:
      - db
networks:
  db:
  front:
    # Doit être crée manuellement
    # docker network create front
    external: true
```

:::
:::

. . .

####
* Un réseau `default` par `docker-compose.yml`, par défaut pour les conteneurs
* `db` est un réseau interne

### Compilation

#### `docker-compose.yml`
```yaml
version: "3"

services:

  front:
    # n'importe quel chemin relatif au `docker-compose.yml`
    build: .
```

. . .

####
* Compile depuis le `Dockerfile` du dossier courant
* Compilation automatique au premier `docker-compose up`


### Comment ajouter de la flexibilité ?

####
+ Séparer les modes production / développement
+ Choisir d'exposer ou non des ports
+ Ajouter une série de configurations optionnelle (ex label pour Traefik)
+ Services optionnels (phpmyadmin dans une couche LAMP)

. . .

#### La variable COMPOSE_FILE

équivalente à l'argument `-f FILE` permet de "chainer" des `docker-compose.yml`


```
COMPOSE_FILE=docker-compose.yml:ports.yml:phpmyadmin.yml
```


# TP

### Consignes globales

+ Si ce n'est pas déjà fait installer `docker-compose` (paquet `apt`)

#### Documentation

* [Référence `docker-compose.yml`](https://docs.docker.com/compose/compose-file/compose-file-v3/)
* [Référence `docker-compose` CLI](https://docs.docker.com/compose/reference/overview/)

### `docker-compose` CLI

1. Reprendre un `docker-compose.yml` avec un service `apache`
2. Lancer `docker-compose up`
3. Accéder au conteneur par le port exposé
4. Chercher dans la documentation comment ouvrir un terminal dans le conteneur avec `docker-compose`
5. Lancer `docker-compose ps`  et `docker ps`
5. Arrêter les services avec `ctrl+C`
6. Lancer `docker-compose down` puis `docker-compose ps -a` que c'est il passé ?
7. Comment lancer les service en arrière plan ?

### Variables d'environnement et volumes

Reproduire le mini-site de la première semaine avec `docker-compose` :

1. Faire un service apache
2. Passer la variable `NOM` par `.env`
3. Faire en sorte qu'il serve le fichier suivant

```{.PHP}
Bonjour
<?php
echo $_ENV['NOM'];
?>
```

4. Changer la variable d'environnement dans le `.env`
5. comment faire pour que le changement soit pris en compte ?


### Réseaux

1. Lancer un `docker-compose` avec deux service
2. Ouvrir un terminal dans chacun des conteneurs
3. "`pinger`" l'autre service
4. Est-ce que cela fonctionne ?
5. Isoler les conteneur dans des réseau séparés
6. Combien de réseau avez vous besoin de créer ?

### Réseaux avancé

1. Lancer deux `docker-compose` avec un service dans chaque
2. Ouvrir un terminal dans chacun des conteneurs
3. "`pinger`" l'autre service
4. Est-ce que cela fonctionne ?
5. Configurer les réseau pour que ces services puissent communiquer

### Compilation locale

1. Faire un `docker-compose.yml` qui compile une image
    * Par exemple partir de `php:appache` et ajouter un `index.html` dans `/var/www/html`
2. Lancer avec `docker-compose up`
3. Modifier le `Dockerfile`
4. Relancer le `docker-compose up`
5. L'image est-elle recompilée ?
6. Si non comment faire pour la recompiler ?


### COMPOSE_FILE

1. Reprendre le mini-site
2. Utiliser le mécanise `COMPOSE_FILE` afin de mettre l'exposition de ports dans un autre fichier `.yml`
3. Est-ce que l'ordre compte dans la variable `COMPOSE_FILE`
4. Est-il possible via `COMPOSE_FILE` de supprimer une ligne du précédent fichier ?
5. Quelle est (sont) à votre avis la (les) bonnes pratiques d'utilisation de `COMPOSE_FILE` ?

### Bonus

Faire un `docker-compose.yml` avec deux services :

1. Un front-end web
2. Un backend qui compile en html tous les fichier markdown d'un dossier et les met dans le dossier servit par le frontend
3. Isoler le backend dans un réseau à part sans accès internet (voir dans la documentation)

# Conclusion

### Conclusion

* Approche 100% descriptive
* Possibilité de versionner une architecture complète avec git
* Possibilité d'exposer facilement les configurations
+ Seule les modifications volontaires de configurations sont visibles
* Mise en cohérence de toute une pile grâce aux variables d'environnement partagées
* Même commande pour gérer différent groupes de services

### Pour aller plus loin

* Seulement quelques instructions présentées voir [la documentation](https://docs.docker.com/compose/compose-file/compose-file-v3/)
* La séance prochaine on parle de
    * Outils pour la production
        * [Træfik](https://traefik.io/traefik/)
        * Sauvegardes
        * Mises à jour
    * Considération de sécurité
    * Autres : dites moi ce qui vous intéresse
